# read in data and manipulate to get a normalized deviance by month for each program

require(dplyr)
events <- read.csv("SnakeChartData.csv", stringsAsFactors=FALSE)
df = bind_rows( 
  events %>% mutate(Date=PlanDate,Amt=PlanAmt,Event="Pl"),
  events %>% mutate(Date=ObligationDate,Amt=ObligationAmt,Event="Ob")
)%>%filter(Date!="NULL",Amt!="$0.00")%>%mutate(
  Y=as.POSIXlt(Date,format="%m/%d/%Y")$year+1900,
  M=as.POSIXlt(Date,format="%m/%d/%Y")$mon+1,
  nAmt=as.numeric(gsub('\\$|,','',Amt))
)%>%group_by(ElementOfCost)%>%filter(
  'Pl'%in% Event #make sure that a plan exists for the EOC, not the same as Event=='Pl'
)%>%mutate(
  #eY=max(Y[Event=='Pl']),
  #eM=max(M[Y==eY & Event=='Pl']),
  #nM=12*(Y-eY)+M-eM,
  nM=-1*(12*(FY+unlist(list("O&M"=1,"PROC"=3,"RDTE"=2)[Appn])-Y-1)+(9-M))
)%>%group_by(EOC=ElementOfCost,nM)%>%summarize(
  Pl=sum((Event=="Pl")*nAmt),
  Ob=sum((Event=="Ob")*nAmt)
)%>%group_by(EOC)%>%arrange(nM)%>%do( #fill in months
  rbind(.,{
    if(length(.$nM)<(max(.$nM)-min(.$nM))){
      r=seq(min(.$nM),max(.$nM));
      a=r[!(r%in%.$nM)];
      b=.[rep(1,length(a)),];
      b[,]=0
      b$nM=a;
      b$EOC=.$EOC[1]
      b
    }else{
      list()
    }
  })%>%arrange(nM)
)%>%mutate(
  fcd=cumsum(Ob-Pl)/cumsum(Pl),
  final=last(fcd)
)%>%filter(fcd<=1)%>%inner_join(
  events%>%select(FY,Appn,P1R1,PE,Org,EOC=ElementOfCost)%>%distinct(),
  by="EOC"
)
save(df,file="data.Rdata")
