const SqliteToJson = require('sqlite-to-json');
const sqlite3 = require('sqlite3');
const exporter = new SqliteToJson({
  client: new sqlite3.Database('../data.sqlite')
});
exporter.save('months', './months.json', function (err) {
  // no error and you're good. 
});
exporter.save('EOCs', './EOCs.json', function (err) {
  // no error and you're good.
});

